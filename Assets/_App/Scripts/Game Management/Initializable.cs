﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    /// <summary>
    /// Generic classes for forced using of constructor
    /// in inherited classes
    /// </summary>
    /// <typeparam name="T1"></typeparam>

    public abstract class Initializable<T1>
    {
        public Initializable(T1 parameters1)
        {

        }

    }

    public abstract class Initializable<T1, T2>
    {
        public Initializable(T1 parameters1, T2 parameters2)
        {

        }

    }

    public abstract class Initializable<T1, T2, T3>
    {
        public Initializable(T1 parameters1, T2 parameters2, T3 parameters3)
        {

        }

    }

    public abstract class Initializable<T1, T2, T3, T4>
    {
        public Initializable(T1 parameters1, T2 parameters2, T3 parameters3, T4 parameters4)
        {

        }

    }

}
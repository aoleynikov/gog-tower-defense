﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public interface IGameController
    {
        void SetupScene(int curLevel);

        void Tick();
    }

}

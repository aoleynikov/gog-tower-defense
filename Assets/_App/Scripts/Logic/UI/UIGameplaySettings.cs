﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    public class UIGameplaySettings : MonoBehaviour
    {
        [SerializeField]
        private Dropdown _modesDropdown;

        // fires when new settings mode applied
        public UnityAction<GameSettingsType> OnModeApplied;

        private void Awake()
        {
            gameObject.SetActive(false);
        }

        public void OnButtonApply()
        {
            GameSettingsType settingsType = (GameSettingsType)_modesDropdown.value;

            OnModeApplied.Invoke(settingsType);
        }

        public void SetCurrentType(int index)
        {
            _modesDropdown.value = index;
        }

    }
}
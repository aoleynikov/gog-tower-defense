﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    public class UIWavePanel : MonoBehaviour
    {
        [SerializeField]
        private Text _waveId;


        public void ShowWavePanel(int waveId)
        {
            _waveId.text = "" + (waveId + 1);
            gameObject.SetActive(true);
            StartCoroutine(ShowPanel());
        }

        IEnumerator ShowPanel()
        {           

            yield return new WaitForSeconds(0.5f);

            gameObject.SetActive(false);
        }
    }
}
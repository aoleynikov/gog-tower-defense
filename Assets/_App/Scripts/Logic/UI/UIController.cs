﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    public class UIController : MonoBehaviour
    {        
        [SerializeField]
        private Text _currentBaseHealth;

        [SerializeField]
        private Text _currentLevel;

        [SerializeField]
        private GameObject _winScreen;

        [SerializeField]
        private GameObject _loseScreen;

        [SerializeField]
        private UIGameplaySettings _uiGameplaySettings;

        [SerializeField]
        private UIMenu _uiMenu;

        [SerializeField]
        private UIShop _uiShop;

        [SerializeField]
        private UIWavePanel _wavePanel;

        [SerializeField]
        private GameObject _statsPanel;

        private GameplaySettingsController _gameplaySettingsController;

        public UnityAction OnReplay;
        public UnityAction OnNextLevel;
        public UnityAction<GameSettingsType> OnModeApplied;
        public UnityAction OnPlayGame;



        public void Setup(ShopConfig shopConfig, ShopController shopController, GameplaySettingsController gameplaySettingsController)
        {
            _gameplaySettingsController = gameplaySettingsController;


            _winScreen.SetActive(false);
            _loseScreen.SetActive(false);

            _uiGameplaySettings.OnModeApplied += SetGameplaySettings;
            _uiGameplaySettings.SetCurrentType((int)_gameplaySettingsController.GetSettingsType());

            _uiMenu.OnPlayGame += PlayGame;
            _uiMenu.gameObject.SetActive(true);

            _uiShop.Setup(shopConfig, shopController);
            _uiShop.gameObject.SetActive(false);

            _statsPanel.SetActive(false);
        }

        private void PlayGame()
        {
            OnPlayGame.Invoke();
            _uiShop.gameObject.SetActive(true);
            _statsPanel.SetActive(true);
        }
               

        public void SetBaseHealth(int baseHealth)
        {
            _currentBaseHealth.text = "" + baseHealth;
        }

        public void ShowUpgradePanel(bool canUpgrade, IUpgradable unitUpgrader)
        {
            _uiShop.ShowUpgradePanel(canUpgrade, unitUpgrader);
        }

        public void HideUpgradePanel(IUpgradable unitUpgrader)
        {
            _uiShop.HideUpgradePanel(unitUpgrader);
        }

        public void CanUseShop()
        {
            _uiShop.CanUseShop();
        }


        public void LevelWin()
        {
            _winScreen.SetActive(true);
        }

        public void LevelLose()
        {
            _loseScreen.SetActive(true);
        }

        public void SetCurrentLevel(int level)
        {
            _currentLevel.text = "" + level;
        }

        public void SetCurrentMoney(int amount)
        {
            _uiShop.SetCurrentMoney(amount);
        }

        public void Replay()
        {
            OnReplay.Invoke();
        }

        public void Next()
        {
            OnNextLevel.Invoke();
        }

        private void SetGameplaySettings(GameSettingsType settingsType)
        {
            OnModeApplied.Invoke(settingsType);
        }

        public void SetPriceAdd(int priceAdd)
        {
            _uiShop.SetPriceAdd(priceAdd);
        }

        public void ShowWavePanel(int waveId)
        {
            _wavePanel.ShowWavePanel(waveId);
        }
    }
}

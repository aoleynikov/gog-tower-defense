﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    public class UIUnit : MonoBehaviour
    {
        [SerializeField]
        private Image _life;

        [SerializeField]
        private Image _armour;

        public void SetLifePercent(float lifePercent)
        {
            _life.fillAmount = lifePercent;
        }

        public void SetArmourPercent(float armourPercent)
        {
            if (_armour != null)
            {
                _armour.fillAmount = armourPercent;
            }
        }
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    public class UIMenu : MonoBehaviour
    {
        public UnityAction OnPlayGame;

        public void OnQuit()
        {
            Application.Quit();
        }

        public void OnPlay()
        {
            if (OnPlayGame != null)
            {
                OnPlayGame.Invoke();
            }
        }
    }
}

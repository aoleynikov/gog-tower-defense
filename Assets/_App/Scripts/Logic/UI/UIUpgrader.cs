﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace GOG.TowerDefense {

    public class UIUpgrader : MonoBehaviour {

        [SerializeField]
        private Text _type;
        [SerializeField]
        private Text _amount;
        [SerializeField]
        private Text _price;
        [SerializeField]
        private Text _currentLevel;
        [SerializeField]
        private Button _upgradeButton;
        

        public UnityAction OnUpgradeClick;

        public void SetType(string type)
        {
            _type.text = type;
        }

        public void SetAmount(string amount)
        {
            _amount.text = "+" + amount;
        }

        public void SetPrice(string price)
        {
            _price.text = "$" + price;
        }

        public void SetCurrentLevel(string currentLevel)
        {
            _currentLevel.text = "Lvl " + currentLevel;
        }

        public void UpgradeClick()
        {
            OnUpgradeClick.Invoke();
        }

        public void BlockUpgradeButton()
        {
            _upgradeButton.interactable = false;
        }

    }

}

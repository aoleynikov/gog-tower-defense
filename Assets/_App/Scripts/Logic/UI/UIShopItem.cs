﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIShopItem : MonoBehaviour {

    public UnityAction<int> OnItemClicked;

    [SerializeField]
    private Text _price;

    private Button button;

    private int _id;
    private float _priceValue;
    
    private void Awake () {

        button = GetComponent<Button>();

        button.onClick.AddListener(ItemClicked);
	}

    private void OnApplicationQuit()
    {
        button.onClick.RemoveAllListeners();
    }
    
    private void ItemClicked()
    {
        OnItemClicked.Invoke(_id);
    }

    public void SetID(int id)
    {
        _id = id;
    }

    public void SetPrice(int price)
    {
        _price.text = "" + price;
        _priceValue = price;
    }

    public void SetPriceAdd(int priceMultiplier)
    {
        _price.text = "$ " + (_priceValue + priceMultiplier);
    }
}

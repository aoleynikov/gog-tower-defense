﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    public class UIShop : MonoBehaviour
    {
        [SerializeField]
        private GameObject _shopPanel;

        [SerializeField]
        private Text _currentMoney;

        [SerializeField]
        private GameObject _towerUpgradePanel;

        [SerializeField]
        private GameObject _itemPrefab;

        [SerializeField]
        private GameObject _upgradePanelPrefab;
        [SerializeField]
        private GameObject _cannotUpdatePrefab;
        [SerializeField]
        private GameObject _noMoneyPrefab;

        private ShopController _shopController;

        private ShopConfig _shopConfig;

        private IUpgradable _currentUnitUpgrader;

        [SerializeField]
        private GameObject _currentUpgradePanel;        
        private GameObject _noMoneyPanel;

        private List<UIShopItem> _shopItems = new List<UIShopItem>();

        private bool _turretBuyProcess = false;


        public void Setup(ShopConfig shopConfig, ShopController shopController)
        {

            _shopConfig = shopConfig;

            _shopController = shopController;
            _shopController.OnCurrentMoney += SetCurrentMoney;

            SetCurrentMoney(_shopController.GetCurrentMoney());

            _towerUpgradePanel.SetActive(false);

            FillShop();
        }

        public void SetCurrentMoney(int money)
        {
            _currentMoney.text = "" + money;
        }

        private void FillShop()
        {
            int id = 0;
            foreach (ISellable sellable in _shopConfig.turretsOnLevel)
            {
                GameObject itemObj = GameObject.Instantiate(_itemPrefab);
                itemObj.GetComponent<Image>().sprite = sellable.GetShopImage();
                itemObj.transform.parent = _shopPanel.transform;

                UIShopItem item = itemObj.GetComponent<UIShopItem>();
                item.SetID(id);
                item.OnItemClicked += TurretItemClicked;
                item.SetPrice(sellable.GetShopPrice());

                _shopItems.Add(item);

                id++;
            }
        }

        private void TurretItemClicked(int id)
        {
            if (!_turretBuyProcess)
            {
                _turretBuyProcess = true;
                if (!_shopController.TryBuyTurret(id))
                {
                    _turretBuyProcess = false;
                }
            }
        }

        public void CanUseShop()
        {
            _turretBuyProcess = false;
        }

        public void ShowUpgradePanel(bool canUpgrade, IUpgradable unitUpgrader)
        {
            if (_currentUnitUpgrader != null)
            {
                HideUpgradePanel(_currentUnitUpgrader);
            }

            _currentUnitUpgrader = unitUpgrader;

            _towerUpgradePanel.SetActive(true);

            if (canUpgrade)
            {
                CreateUpgradeType();
            }
            else
            {
                CannotUpgradeMessage();
            }

        }


        public void HideUpgradePanel(IUpgradable unitUpgrader)
        {
            if (unitUpgrader == null || _currentUnitUpgrader == unitUpgrader)
            {
                _towerUpgradePanel.SetActive(false);

                _currentUnitUpgrader = null;

                DestroyUpgradeType();
            }
        }

        public void SetPriceAdd(int priceAdd)
        {
            foreach (UIShopItem item in _shopItems)
            {
                item.SetPriceAdd(priceAdd);
            }
        }

        private void CreateUpgradeType()
        {
            _currentUpgradePanel = GameObject.Instantiate(_upgradePanelPrefab);
            _currentUpgradePanel.transform.SetParent(_towerUpgradePanel.transform);

            UpgradeConfig descriptor = _currentUnitUpgrader.GetDescriptor();

            _currentUpgradePanel.GetComponent<UIUpgrader>().SetType(descriptor.UpgradeType.ToString());
            _currentUpgradePanel.GetComponent<UIUpgrader>().SetAmount(descriptor.UpgradeAmount.ToString());
            _currentUpgradePanel.GetComponent<UIUpgrader>().SetPrice(descriptor.UpgradePrice.ToString());
            _currentUpgradePanel.GetComponent<UIUpgrader>().SetCurrentLevel(_currentUnitUpgrader.GetCurretLevel().ToString());

            _currentUpgradePanel.GetComponent<UIUpgrader>().OnUpgradeClick += UpgradeClick;


            // add message if not enough money
            if (!_shopController.HasEnoughMoney(descriptor.UpgradePrice, false))
            {
                _noMoneyPanel = GameObject.Instantiate(_noMoneyPrefab);
                _noMoneyPanel.transform.SetParent(_towerUpgradePanel.transform);

                _currentUpgradePanel.GetComponent<UIUpgrader>().BlockUpgradeButton();
            }
        }


        private void CannotUpgradeMessage()
        {
            _currentUpgradePanel = GameObject.Instantiate(_cannotUpdatePrefab);
            _currentUpgradePanel.transform.SetParent(_towerUpgradePanel.transform);
        }

        private void DestroyUpgradeType()
        {
            if (_currentUpgradePanel.GetComponent<UIUpgrader>() != null && _currentUpgradePanel.GetComponent<UIUpgrader>().OnUpgradeClick != null)
                _currentUpgradePanel.GetComponent<UIUpgrader>().OnUpgradeClick -= UpgradeClick;
            if (_currentUpgradePanel != null)
                Destroy(_currentUpgradePanel);
            if (_noMoneyPanel != null)
            {
                Destroy(_noMoneyPanel);
            }

            _currentUnitUpgrader = null;
        }

        private void UpgradeClick()
        {
            if (_shopController.TryUpgradeTurret(_currentUnitUpgrader.GetDescriptor().UpgradePrice))
            {
                _currentUnitUpgrader.MakeUpgrade();
            }

            HideUpgradePanel(_currentUnitUpgrader);
        }

        public void CloseUpgradePanel()
        {
            HideUpgradePanel(_currentUnitUpgrader);
        }


    }
}
﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public enum GameSettingsType
    {
        Easy = 0,
        Normal = 1,
        Hard = 2
    }

    [Serializable]
    public class GameplaySettings
    {
        public GameSettingsType gameplaySettingsType = GameSettingsType.Normal;
        [Range(0.1f, 10)]
        public float enemiesSpeedMultiplier = 1;
        [Range(1, 10)]
        public int enemiesCountMultiplier = 1;
    }

    public class GameplaySettingsController : Initializable<GameplaySettingsConfig>
    {
        private GameSettingsType _currentSettingsType = GameSettingsType.Normal;
        private GameplaySettingsConfig _settingsConfig;

        public GameplaySettingsController(GameplaySettingsConfig settingsConfig) : base(settingsConfig)
        {
            if (PlayerPrefs.HasKey("GameSettingsType")) _currentSettingsType = (GameSettingsType)PlayerPrefs.GetInt("GameSettingsType");

            _settingsConfig = settingsConfig;
        }

        public void SetSettingsType(GameSettingsType currentSettingsType)
        {
            _currentSettingsType = currentSettingsType;
            PlayerPrefs.SetInt("GameSettingsType", (int)currentSettingsType);
        }

        public GameSettingsType GetSettingsType()
        {
            return _currentSettingsType;
        }

        public GameplaySettings GetSettings()
        {     
            foreach (GameplaySettings settings in _settingsConfig.gameplaySettings)
            {
                if (settings.gameplaySettingsType.Equals(_currentSettingsType))
                {
                    return settings;
                }
            }

            return null;
        }


    }
}

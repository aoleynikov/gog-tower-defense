﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense {

    public class TurretUpgrader : MonoBehaviour, IUpgradable
    {
        private int _currentLevel = 0;

        public UnityAction<UpgradeConfig> OnUpgraded;

        [SerializeField]
        private List<UpgradeConfig> _upgradeLevels = new List<UpgradeConfig>();


        public int GetCurretLevel()
        {
            return _currentLevel;
        }

        public UpgradeConfig GetDescriptor()
        {
            return _upgradeLevels[_currentLevel];
        }

        /// <summary>
        /// When user upgraded turret in UI
        /// </summary>
        public void MakeUpgrade()
        {
            if (_currentLevel < _upgradeLevels.Count)
            {
                if (OnUpgraded != null)
                    OnUpgraded.Invoke(GetDescriptor());
                _currentLevel++;
            }
        }   

        public UpgradeConfig GetCurrentUpdateDescriptor()
        {
            //Debug.LogError("_currentLevel = " + _currentLevel + " _upgradeLevels.Count = " + _upgradeLevels.Count + " _upgradeLevels[_currentLevel] = " + _upgradeLevels[_currentLevel]);

            if (_currentLevel < _upgradeLevels.Count )
            {
                return _upgradeLevels[_currentLevel];
            }

            return null;
        }

       
    }

}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


namespace GOG.TowerDefense
{
    public class Turret : MonoBehaviour, IUnit, IPlaceable, ISellable
    {
        [SerializeField]
        private TurretConfig _turretConfig;

        [SerializeField]
        private SeekAim _seekAim;

        [SerializeField]
        private Shooter _shooter;

        [SerializeField]
        private TakeDamage _damager;

        [SerializeField]
        private TurretUpgrader _turretUpgader;

        [SerializeField]
        private TurretPlacement _turretPlacement;

        [SerializeField]
        private UIUnit _turretUI;

        public UnityAction<TurretUpgrader> OnTurretUpgrade;
        public UnityAction<Turret> OnTurretDestroy;
        public UnityAction<Turret, bool> OnTurretPlaced;

        public TurretPlacement TurretPlacement { get { return _turretPlacement; } }
        
        private void Awake()
        {
            if (_turretPlacement == null)
                _turretPlacement = GetComponentInChildren<TurretPlacement>();

            _turretPlacement.Construct(this);
            _turretPlacement.OnTurretPlaced += TurretPlaced;

            if (_damager == null)
            {
                _damager = GetComponentInChildren<TakeDamage>();
            }

            _damager.OnDamage += DamageTurret;
            _damager.OnDestroy += DestroyTurret;

            _damager.Init();

            if (_turretUpgader == null)
            {
                _turretUpgader = GetComponentInChildren<TurretUpgrader>();
            }


            _turretUpgader.OnUpgraded += TurretUpdated;

            gameObject.name = "Turret_" + Time.time;
        }        

        private void TurretPlaced(bool placed)
        {
            _seekAim.CanSeek = true;
            _shooter.CanShoot = true;
            _damager.CanTakeDamage = true;

            OnTurretPlaced.Invoke(this, placed);
        }

        public void DestroyTurret()
        {
            _turretPlacement.FreeCell();

            OnTurretDestroy.Invoke(this);
            Destroy(gameObject);
        }

        public void DamageTurret(float lifePercent, float armourPercent)
        {
            _turretUI.SetLifePercent(lifePercent);
            _turretUI.SetArmourPercent(armourPercent);
        }

        /// <summary>
        /// When user clicks on turret but not upgraded yet
        /// </summary>
        public void WantToUpdateTurret()
        {
            OnTurretUpgrade.Invoke(_turretUpgader);
        }

        /// <summary>
        /// When User updated turret
        /// </summary>
        /// <param name="updateConfig"></param>
        private void TurretUpdated(UpgradeConfig updateConfig)
        {
            if (updateConfig.UpgradeType.Equals(UpgradeType.Distance))
            {
                _shooter.IncreaseRange(updateConfig.UpgradeAmount);
            }
            else if (updateConfig.UpgradeType.Equals(UpgradeType.Force))
            {
                _shooter.IncreaseForce(updateConfig.UpgradeAmount);
            }
            else if (updateConfig.UpgradeType.Equals(UpgradeType.Armour))
            {
                _damager.AddArmour(updateConfig.UpgradeAmount);
            }
        }

        public IUpgradable GetUpgrader()
        {
            return _turretUpgader;
        }

        public Sprite GetShopImage()
        {
            return _turretConfig.shopImage;
        }

        public int GetShopPrice()
        {
            return _turretConfig.shopPrice;
        }
    }

}

﻿
using System;

namespace GOG.TowerDefense
{

    public enum UpgradeType
    {
        Distance, // upgrade of shooting distance
        Force, // upgrade of shooting force
        Armour // upgrade of armour amount
    }

}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    public class TurretPlacement : MonoBehaviour
    {
        [SerializeField]
        private LayerMask _layer;
        [SerializeField]
        private Vector3 _displacement;


        private Turret _turret;

        private bool _placed = false;
        private bool _cellClicked = false;
        private float _rayDist = 2;

        // potentially to be placed on
        private Cell _cellToPlace = null;
        // finally placed on
        private Cell _placedCell;

        public UnityAction<bool> OnTurretPlaced;

        private void Update()
        {
            if (!_placed)
            {
                Vector3 positionOnPlane = PointOnPlaneFromRay(Camera.main.transform.position, -Camera.main.ScreenPointToRay(Input.mousePosition).direction);
                _turret.transform.position = positionOnPlane;

                RaycastAtDirection(Vector3.down);
            }

            // TODO move into another input controller
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (!_placed)
                {
                    OnTurretPlaced.Invoke(false);

                    Destroy(_turret.gameObject);
                }
            }
        }

        private Vector3 PointOnPlaneFromRay(Vector3 rayStart, Vector3 rayDir)
        {
            float angle = Vector3.Angle(-rayDir, Vector3.up);
            angle *= Mathf.Deg2Rad;
            float len = rayStart.y / Mathf.Cos(angle) + _rayDist;

            return (rayStart + (rayDir * len));
        }

        public void Construct(Turret turret)
        {
            _turret = turret;
        }

        public void CellPlaced(Cell cell)
        {

            _placed = true;

            _placedCell = cell;

            _turret.gameObject.transform.position = cell.transform.position + _displacement;

            OnTurretPlaced.Invoke(true);
            
        }

        public void FreeCell()
        {
            _placedCell.RemoveObjectFromCell();
        }

        private void RaycastAtDirection(Vector3 direction)
        {
            float distance = _rayDist;

            RaycastHit[] hits = Physics.RaycastAll(_turret.transform.position, direction, distance, _layer);

            Debug.DrawLine(_turret.transform.position, _turret.transform.position + direction * distance, UnityEngine.Color.green);

            foreach (RaycastHit hit in hits)
            {
                if (hit.collider != null)
                {
                    _cellToPlace = hit.collider.gameObject.GetComponent<Cell>();

                    Debug.DrawLine(_turret.transform.position, _turret.transform.position + direction * distance, UnityEngine.Color.red);

                    _cellToPlace.OnTurretOver();

                    return;
                }
            }

            _cellToPlace = null;
        }

        public void OnMouseDown()
        {
            // TODO input into another component
            if (!_placed && _cellToPlace)
            {               
                bool canPlace = _cellToPlace.OnTurretTryPlace(_turret);

                if (canPlace)
                {
                    CellPlaced(_cellToPlace);
                }
            }
            else if (_placed)
            {
                _turret.WantToUpdateTurret();
            }
        }

    }

}

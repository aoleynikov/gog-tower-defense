﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense {

    public class TurretsController : Initializable<GameController>
    {
        private GameController _gameController;

        public TurretsController(GameController gameController) : base(gameController)
        {
            _gameController = gameController;
        }

        public void CreateTurret(GameObject turretPrefab) 
        {
            GameObject turret = GameObject.Instantiate(turretPrefab);

            turret.GetComponent<Turret>().OnTurretPlaced += TurretPlaced;
        }

        public void TurretPlaced(Turret turret, bool placed)
        {
            turret.OnTurretUpgrade += _gameController.UpgradeTurret;
            turret.OnTurretDestroy += _gameController.DestroyTurret;

            _gameController.CanUseShop(placed);

        }


    }

}

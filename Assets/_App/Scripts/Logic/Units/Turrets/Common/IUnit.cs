﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public interface IUnit
    {
        IUpgradable GetUpgrader();        
    }

}

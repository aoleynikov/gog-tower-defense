﻿namespace GOG.TowerDefense
{
    public interface IUpgradable
    {
        UpgradeConfig GetDescriptor();
        void MakeUpgrade();
        UpgradeConfig GetCurrentUpdateDescriptor();
        int GetCurretLevel();
    }

}

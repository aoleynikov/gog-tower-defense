﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{

    public interface ISellable
    {
        int GetShopPrice();
        Sprite GetShopImage();
    }
}

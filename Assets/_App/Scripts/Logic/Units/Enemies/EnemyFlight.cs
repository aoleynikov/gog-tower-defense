﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [RequireComponent(typeof(BaseEnemy))]
    public class EnemyFlight : MonoBehaviour
    {
        [SerializeField]
        private float _height = 0;
        private BaseEnemy _enemy;
  
        private void Awake()
        {
            _enemy = gameObject.GetComponent<BaseEnemy>();

            _enemy.Displacement = new Vector3(0, _height, 0);
        }
    }
}
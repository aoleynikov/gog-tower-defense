﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    public abstract class BaseEnemy : MonoBehaviour
    {
        [SerializeField]
        protected NavMeshAgent _navMeshAgent;
        
        [SerializeField]
        protected IWayPointController _wayPointController;

        [SerializeField]
        protected IWavesController _wavesController;

        [SerializeField]
        protected UIUnit _enemyUI;

        [SerializeField]
        protected TakeDamage _damager;

        [SerializeField]
        protected Shooter _shooter;

        [SerializeField]
        protected EnemyConfig _enemyConfig;

        protected int _waypointId = 0;
        protected Transform _target;
        protected Vector3 _displacement = new Vector3(0, 0, 0);

        public IWavesController WavesController { get { return _wavesController; } }
        public int WayPointID { get { return _waypointId;  } }
        public Vector3 Position { get { return transform.position; } }
        public Vector3 Displacement { set { _displacement = value; } }
        public EnemyConfig EnemyConfig { get { return _enemyConfig;  } }

        public UnityAction<int> OnHitBase;
        //public UnityAction OnEnemyCrashedByUser;
        public UnityAction<BaseEnemy> OnEnemyWayFinished;
        public UnityAction<BaseEnemy> OnEnemyKilled;

        public abstract void DamageEnemy(float lifePercent, float armour = 0);
        public abstract void DestroyEnemy();
        public abstract void DestroyEnemyWhenFinish();
        public abstract void StartFromPointIndex(int wayPointId);
        public abstract void StartFromPosition(Vector3 position);

        public virtual void Construct(IWayPointController wayPointController, IWavesController wavesController, float speedMultiplier = 1)
        {
            _wayPointController = wayPointController;
            _wavesController = wavesController;

            if (_damager == null)
            {
                _damager = GetComponentInChildren<TakeDamage>();
            }

            _damager.OnDamage += DamageEnemy;
            _damager.OnDestroy += DestroyEnemy;

            // setup way points
            _waypointId = 0;
            _target = _wayPointController.WayPoints[0];
            transform.position = _target.position;
        }

        
    }
}

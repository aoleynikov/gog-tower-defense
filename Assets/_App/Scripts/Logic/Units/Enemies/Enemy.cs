﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public class Enemy : BaseEnemy
    {        
        private bool _pathFinished = false;

        public override void Construct(IWayPointController wayPointController, IWavesController wavesController, float speedMultiplier = 1)
        {
            base.Construct(wayPointController, wavesController);                       

            // setup nav mesh agent
            _navMeshAgent.destination = _target.position;
            _navMeshAgent.baseOffset = _displacement.y;
            _navMeshAgent.speed = _enemyConfig.speed * speedMultiplier;
        }

        public override void StartFromPointIndex(int wayPointId)
        {
            _waypointId = wayPointId;

            _target = _wayPointController.WayPoints[_waypointId];
            _navMeshAgent.destination = _target.position;
            _navMeshAgent.baseOffset = _displacement.y;
        }

        public override void StartFromPosition(Vector3 position)
        {
            transform.position = position;
        }

        private void OnDisable()
        {
            _damager.OnDamage -= DamageEnemy;
            _damager.OnDestroy -= KillEnemy;
        }

        private void Update()
        {
            // lock on target if shooting or if no gun
            if ((_shooter != null && !_shooter.IsShooting()) || (_shooter == null))
            {
                LockOnTarget(_target);
            }

            if (Vector3.Distance(transform.position, _target.position) <= 3f)
            {
                GetNextWaypoint();
            }
        }

        public void LockOnTarget(Transform target)
        {
            Vector3 dir = transform.position - target.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);
            Vector3 rotation = Quaternion.Lerp(transform.rotation, lookRotation, Time.deltaTime * _enemyConfig.turnSpeed).eulerAngles;
            transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
        }

        void GetNextWaypoint()
        {
            if (_waypointId >= _wayPointController.WayPoints.Length - 1)
            {
                EndPath();
                return;
            }

            _waypointId++;
            _target = _wayPointController.WayPoints[_waypointId];

            _navMeshAgent.destination = _target.position ;
            _navMeshAgent.baseOffset = _displacement.y;
        }

        void EndPath()
        {
            if (OnHitBase != null)
            {
                OnHitBase.Invoke(_enemyConfig.baseHitDamage);
            }

            _pathFinished = true;

            DestroyEnemyWhenFinish();
        }

        public override void DamageEnemy(float lifePercent, float armour)
        {
            _enemyUI.SetLifePercent(lifePercent);
            _enemyUI.SetArmourPercent(armour);
        }

        public override void DestroyEnemy()
        {
            OnEnemyKilled.Invoke(this);
            KillEnemy();
        }

        public override void DestroyEnemyWhenFinish()
        {
            OnEnemyWayFinished.Invoke(this);
            KillEnemy();                      
        }

        private void KillEnemy()
        {
            

            if (gameObject != null)
                Destroy(gameObject);
        }
    }
}

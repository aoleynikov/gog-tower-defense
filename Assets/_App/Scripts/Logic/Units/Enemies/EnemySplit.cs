﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [RequireComponent(typeof(BaseEnemy))]
    public class EnemySplit : MonoBehaviour
    {
        [SerializeField]
        private EnemySplitConfig _enemySplitConfig;

        private BaseEnemy _enemy;
        private IWavesController _wavesController;

        private void Start()
        {
            _enemy = gameObject.GetComponent<BaseEnemy>();
            _enemy.OnEnemyKilled += SplitEnemy;
            _wavesController = _enemy.WavesController;
        }

        private void SplitEnemy(BaseEnemy enemy)
        {
            for (int i = 0; i < _enemySplitConfig.count; i++)
            {
                BaseEnemy splittedEnemy = _wavesController.SpawnEnemy(_enemySplitConfig.splittedPrefab);
                splittedEnemy.StartFromPointIndex(_enemy.WayPointID);
                splittedEnemy.StartFromPosition(_enemy.Position);
            }
        }

    }
}
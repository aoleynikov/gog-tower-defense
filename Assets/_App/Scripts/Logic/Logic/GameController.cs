﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace GOG.TowerDefense
{
    /// <summary>
    /// The heart of game logic
    /// </summary>
    public class GameController : Initializable<GameConfig, AsyncProcessor, UIController, SoundPlayer>, IGameController
    {
        // game config — scriptable object
        private GameConfig _gameConfig;
        // async processor for access to coroutines
        private AsyncProcessor _asyncProcessor;

        // waypoints controller of current level
        private WaypointsController _wayPointsController;
        // waves controller
        private WavesController _wavesController;

        // shop contoller
        private ShopController _shopController;
        // turrets controller
        private TurretsController _turretsController;

        // ui controller passed from outside (from game manager)
        private UIController _uiController;

        // sounds controller
        private SoundPlayer _soundPlayer;

        // gameplay settings controller
        private GameplaySettingsController _gameplaySettingsController;

        // level to run
        private int _curLevel = 0;

        // current base health
        private int _baseHealth;
        // maximum base health
        private int _maxBaseHealth;
        

        public GameController(  GameConfig gameConfig, 
                                AsyncProcessor asyncProcessor, 
                                UIController uiController, 
                                SoundPlayer soundPlayer) : base(gameConfig, asyncProcessor, uiController, soundPlayer)
        {
            _gameConfig = gameConfig;
            _asyncProcessor = asyncProcessor;
            _uiController = uiController;
            _soundPlayer = soundPlayer;
        }

        public void SetupScene(int curLevel)
        {
            _curLevel = curLevel;
            _maxBaseHealth = _gameConfig.levels[_curLevel].maxBaseLives;
            _baseHealth = _maxBaseHealth;
            
            // create new gameplay settings controller
            _gameplaySettingsController = new GameplaySettingsController(_gameConfig.gameplaySettingsConfig);

            // create new turrets controller
            _turretsController = new TurretsController(this);

            // create new shop controller
            _shopController = new ShopController(_gameConfig.levels[_curLevel], _turretsController);

            // setup UI controller
            _uiController.Setup(_gameConfig.levels[_curLevel].shopConfig, _shopController, _gameplaySettingsController);
            _uiController.SetCurrentLevel(_curLevel + 1);
            _uiController.SetBaseHealth(_maxBaseHealth);

            _uiController.OnNextLevel += RunNextLevel;
            _uiController.OnReplay += ReplayLevel;
            _uiController.OnModeApplied += SetGameplaySettings;
            _uiController.OnPlayGame += PlayGame;

            // instantiate waypoints of the level and setup waypoints controller
            GameObject wayPoints = GameObject.Instantiate(_gameConfig.levels[_curLevel].waypointsController.gameObject);
            _wayPointsController = wayPoints.GetComponent<WaypointsController>();

            // setup waves controller
            _wavesController = new WavesController(_gameConfig.levels[_curLevel].wavesOnLevel,
                                                   _gameplaySettingsController,
                                                   _asyncProcessor,
                                                   _wayPointsController);

            _wavesController.OnHitBase += HitBase;
            _wavesController.OnWavesFinish += WavesFinish;
            _wavesController.OnKillEnemy += EnemyKilled;
            _wavesController.OnNewWave += NewWave;                     
        }

        /// <summary>
        /// Called on new wave
        /// </summary>
        /// <param name="waveId"></param>
        /// <param name="priceAdd"></param>
        private void NewWave(int waveId, int priceAdd)
        {
            _uiController.ShowWavePanel(waveId);

            _uiController.SetPriceAdd(priceAdd);
            _shopController.SetPriceAdd(priceAdd);
        }

        /// <summary>
        /// Called when play game called in UI
        /// </summary>
        private void PlayGame()
        {
            // Start game
            _wavesController.SpawnWaves();
            _soundPlayer.Play(true);
        }

        /// <summary>
        /// Set current gameplay settings
        /// </summary>
        /// <param name="settingsType"></param>
        private void SetGameplaySettings(GameSettingsType settingsType)
        {
            _gameplaySettingsController.SetSettingsType(settingsType);
            _wavesController.SetGameplaySettings();
        }

        /// <summary>
        /// Called when all waves finished
        /// </summary>
        private void WavesFinish()
        {
            if (_baseHealth > 0)
            {
                _uiController.LevelWin();

                _soundPlayer.Stop();
            }
        }

        /// <summary>
        /// Called when enemy killed
        /// </summary>
        /// <param name="enemy"></param>
        private void EnemyKilled(BaseEnemy enemy)
        {
            _shopController.AddMoney(enemy.EnemyConfig.moneyBonusForKill);
            _uiController.SetCurrentMoney(_shopController.GetCurrentMoney());
        }

        /// <summary>
        /// Calls when enemy hits bese
        /// </summary>
        /// <param name="damage"></param>
        private void HitBase(int damage)
        {
            _baseHealth -= damage;
            _baseHealth = Mathf.Clamp(_baseHealth, 0, _maxBaseHealth);

            _uiController.SetBaseHealth(_baseHealth);

            if (_baseHealth == 0)
            {
                _wavesController.Stop();

                _uiController.LevelLose();

                _soundPlayer.Stop();
            }           
        }

        public void Tick()
        {           
        }

        /// <summary>
        /// Calls when upgrade of turret is wanted
        /// </summary>
        /// <param name="upgrader"></param>
        public void UpgradeTurret(IUpgradable upgrader)
        {
            if (upgrader.GetCurrentUpdateDescriptor() != null) {              

                _uiController.ShowUpgradePanel(true, upgrader);
            }
            else
            {
                _uiController.ShowUpgradePanel(false, upgrader);
            }

        }

        /// <summary>
        /// If turret is killed while trying to upgrade it — hide upgrade panel
        /// </summary>
        /// <param name="unit"></param>
        public void DestroyTurret(IUnit unit)
        {
            _uiController.HideUpgradePanel(unit.GetUpgrader());
        }

        /// <summary>
        /// If we want to replay level
        /// </summary>
        public void ReplayLevel()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }

        /// <summary>
        /// If we want to run next level
        /// </summary>
        public void RunNextLevel()
        {
            if (_curLevel + 1 < _gameConfig.levels.Count)
            {
                SceneManager.LoadScene(_gameConfig.levels[_curLevel + 1].scenePath);
            }
        }

        /// <summary>
        /// We can use shop if turret placed / or placement canceled
        /// </summary>
        /// <param name="placed"></param>
        public void CanUseShop(bool placed)
        {
            _uiController.CanUseShop();

            _uiController.SetCurrentMoney(_shopController.GetCurrentMoney());

            if (!placed)
            {
                _shopController.CancelBuy();
            }
        }
    }
}

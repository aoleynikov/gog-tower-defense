﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public interface IWavesController
    {
        void SpawnWaves();

        BaseEnemy SpawnEnemy(BaseEnemy enemy);
    }
}
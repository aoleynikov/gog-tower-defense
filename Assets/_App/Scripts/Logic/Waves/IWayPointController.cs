﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public interface IWayPointController
    {
        void FindWaypoints();
        Transform[] WayPoints { get; }
    }
}

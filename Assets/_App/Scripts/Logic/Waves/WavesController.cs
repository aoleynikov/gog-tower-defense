﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    public class WavesController : Initializable<List<WaveOnLevelConfig>, GameplaySettingsController, AsyncProcessor, WaypointsController>, IWavesController
    {
        private List<WaveOnLevelConfig> _wavesOnLevelConfigs;
        private GameplaySettingsController _gameplaySettingsController;
        private AsyncProcessor _asyncProcessor;
        private WaypointsController _wayPointsController;
        private Coroutine _waveCoroutine;

        private List<BaseEnemy> _activeEnemies = new List<BaseEnemy>();

        private GameplaySettings _gameplaySettings;

        public UnityAction<int> OnHitBase;
        public UnityAction OnWavesFinish;
        public UnityAction<BaseEnemy> OnKillEnemy;
        public UnityAction<int, int> OnNewWave;

        private bool _noMoreWaves = false;
        private int _waveId = 0;

        public WavesController(List<WaveOnLevelConfig> wavesOnLevelConfigs, 
                                GameplaySettingsController gameplaySettingsController,
                                AsyncProcessor asyncProcessor, 
                                WaypointsController wayPointsController) : base(wavesOnLevelConfigs, gameplaySettingsController, asyncProcessor, wayPointsController)
        {
            _wavesOnLevelConfigs = wavesOnLevelConfigs;
            _gameplaySettingsController = gameplaySettingsController;
            _asyncProcessor = asyncProcessor;
            _wayPointsController = wayPointsController;

            _gameplaySettings = _gameplaySettingsController.GetSettings();
        }

        public void SpawnWaves()
        {
            _waveCoroutine = _asyncProcessor.StartCoroutine(SpawnWavesCoroutine());
        }

        private IEnumerator SpawnWavesCoroutine()
        {
            for (int waveId = 0; waveId < _wavesOnLevelConfigs.Count; waveId++)
            {                
                WaveOnLevelConfig wave = _wavesOnLevelConfigs[waveId];
                _waveId = waveId;

                OnNewWave.Invoke(_waveId, wave.turretsPriceAdd);

                for (int i = 0; i < wave.enemiesCount * _gameplaySettings.enemiesCountMultiplier; i++)
                {
                    int enemyIndex = UnityEngine.Random.Range(0, wave.enemies.Count);

                    SpawnEnemy(wave.enemies[enemyIndex]);
                    yield return new WaitForSeconds(1f / wave.rate);
                }

                if (waveId == _wavesOnLevelConfigs.Count - 1)
                {
                    _noMoreWaves = true;
                }

                yield return new WaitForSeconds(_wavesOnLevelConfigs[waveId].pause);
                
            }


        }

        public BaseEnemy SpawnEnemy(BaseEnemy enemy)
        {
            BaseEnemy spawnedEnemy = GameObject.Instantiate(enemy.gameObject).GetComponent<BaseEnemy>();
            spawnedEnemy.Construct(_wayPointsController, this, _wavesOnLevelConfigs[_waveId].speedMultiplier + _gameplaySettings.enemiesSpeedMultiplier);

            spawnedEnemy.OnHitBase += HitBase;
            spawnedEnemy.OnEnemyKilled += EnemyKilled;
            spawnedEnemy.OnEnemyWayFinished += RemoveEnemy;

            _activeEnemies.Add(spawnedEnemy);

            return spawnedEnemy;
        }

        public void HitBase(int damage)
        {
            OnHitBase.Invoke(damage);
        }

        public void Stop()
        {
            _asyncProcessor.StopCoroutine(_waveCoroutine);

            RemoveAllEnemies();
        }

        private void EnemyKilled(BaseEnemy enemy)
        {
            OnKillEnemy.Invoke(enemy);
            RemoveEnemy(enemy);
        }

        public void RemoveEnemy(BaseEnemy enemy)
        {
            // remove passed enemy from the list
            for (int i = _activeEnemies.Count - 1; i >= 0; i--)
            {
                if (_activeEnemies[i] != null && _activeEnemies[i] == enemy)
                {
                    _activeEnemies.RemoveAt(i);
                }
                else if (_activeEnemies[i] == null)
                {
                    _activeEnemies.RemoveAt(i);
                }
            }

            // invoke finish of waves if no more waves and no more enemies
            if (_noMoreWaves && _activeEnemies.Count == 0)
            {
                if (OnWavesFinish != null)
                {
                    OnWavesFinish.Invoke();
                }
            }
        }

        public void SetGameplaySettings()
        {
            _gameplaySettings = _gameplaySettingsController.GetSettings();
        }

        public void RemoveAllEnemies()
        {
            for (int i = _activeEnemies.Count - 1; i >= 0; i--)
            {
                try
                {
                    _activeEnemies[i].DestroyEnemyWhenFinish();
                }
                catch (Exception e)
                {
                    Debug.LogError(e);
                }
            }

            _activeEnemies.Clear();

        }

 
    }

}

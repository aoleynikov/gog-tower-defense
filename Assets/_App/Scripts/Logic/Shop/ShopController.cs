﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    public class ShopController : Initializable<LevelConfig, TurretsController>
    {
        private LevelConfig _levelConfig;
        private ShopConfig _shopConfig;
        private PlayerConfig _playerConfig;
        private TurretsController _turretController;

        public UnityAction<int> OnCurrentMoney;

        private int _currentMoney = 0;

        private int _previousBuyPrice = 0;

        private int _priceAdd = 0;

        public ShopController(LevelConfig levelConfig, TurretsController turretController) : base(levelConfig, turretController)
        {
            _levelConfig = levelConfig;
            _shopConfig = levelConfig.shopConfig;
            _playerConfig = _levelConfig.playerConfig;

            _turretController = turretController;

            _currentMoney = _playerConfig.startMoney;

        }

        public bool TryBuyTurret(int id)
        {
            int turrentPrice = _shopConfig.turretsOnLevel[id].GetShopPrice();

            if (HasEnoughMoney(turrentPrice, true))
            {
                Buy(turrentPrice, true);

                _turretController.CreateTurret(_shopConfig.turretsOnLevel[id].gameObject);

                return true;
            }

            return false;
        }

        public bool TryUpgradeTurret(int price)
        {
            if (HasEnoughMoney(price, false))
            {
                Buy(price, false);
                return true;
            }

            return false;
        }

        public bool HasEnoughMoney(int price, bool usePriceAdd)
        {
            if (usePriceAdd && _currentMoney - price - _priceAdd >= 0)
            {
                return true;
            }
            else if (!usePriceAdd && _currentMoney - price >= 0)
            {
                return true;
            }

            return false;
        }

        private void Buy(int price, bool usePriceAdd)
        {
            if (usePriceAdd)
            {
                _previousBuyPrice = price + _priceAdd;
                _currentMoney -= (price + _priceAdd);
            }
            else
            {
                _previousBuyPrice = price;
                _currentMoney -= price;
            }

            OnCurrentMoney.Invoke(_currentMoney);
        }

        public int GetCurrentMoney()
        {
            return _currentMoney;
        }

        public void AddMoney(int amount)
        {
            _currentMoney += amount;
        }

        public void CancelBuy()
        {
            _currentMoney += _previousBuyPrice;
        }

        public void SetPriceAdd(int priceAdd)
        {
            _priceAdd = priceAdd;
        }
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TakeDamage : MonoBehaviour {

    [SerializeField]
    private int _health = 200;

    public UnityAction OnDestroy;

    public UnityAction<float, float> OnDamage;

    private int _maxHealth;

    [SerializeField]
    private int _armour = 0;

    private int _maxArmour = 0;

    [SerializeField]
    private bool _canTakeDamage = false;

    public bool CanTakeDamage { get { return _canTakeDamage; } set { _canTakeDamage = value; } }

    private void Awake()
    {
        _maxHealth = _health;
        _maxArmour = _armour;
    }

    public void Init()
    {
        if (OnDamage != null)
        {
            if (_armour == 0)
            {
                OnDamage.Invoke(1, 0);
            }
            else
            {
                OnDamage.Invoke(1, 1);
            }
        }
    }

    public void Damage(int damage)
    {
        if (_canTakeDamage)
        {
            int restDamage = -damage;

            if (_armour > 0)
            {
                restDamage = _armour - damage;
                _armour -= damage;
            }

            if (restDamage < 0)
                _health += restDamage;

            _health = Mathf.Clamp(_health, 0, _maxHealth);

            if (_health == 0)
            {
                if (OnDestroy != null)
                    OnDestroy.Invoke();
            }
            else
            {
                float lifePercent = (float)_health / (float)_maxHealth;

                float armourPercent = (float)_armour / (float)_maxArmour;

                if (OnDamage != null)
                    OnDamage.Invoke(lifePercent, armourPercent);
            }
        }
    }

    public void AddArmour(int amount)
    {
        _armour += amount;

        _maxArmour = _armour;

        if (OnDamage != null)
            OnDamage.Invoke((float)_health / (float)_maxHealth, 1);
    }
}

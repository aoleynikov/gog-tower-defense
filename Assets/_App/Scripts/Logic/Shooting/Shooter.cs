﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GOG.TowerDefense
{
    [RequireComponent(typeof(SeekAim), typeof(SphereCollider))]
    public class Shooter : MonoBehaviour
    {
        [SerializeField]
        private float fireRate = 1f;

        [SerializeField]
        private float turnSpeed = 10f;

        [SerializeField]
        private List<Gun> _guns;

        [SerializeField]
        private List<GameObject> _gunBodies;

        private float _fireCountdown = 0f;

        [SerializeField]
        private float _range = 7;

        private SeekAim _aimSeeker;

        [SerializeField]
        private Transform _target;

        [SerializeField]
        private bool _canShoot = false;

        public bool CanShoot { get { return _canShoot; } set { _canShoot = value; } }

        public UnityEvent OnShot;

        void Awake()
        {
            _aimSeeker = GetComponent<SeekAim>();

            _range = GetComponent<SphereCollider>().radius;

            InvokeRepeating("UpdateTarget", 0f, 0.5f);
        }

        void UpdateTarget()
        {
            float shortestDistance = Mathf.Infinity;
            GameObject nearestEnemy = null;
            foreach (GameObject enemy in _aimSeeker.Aims)
            {
                if (enemy != null)
                {
                    float distanceToEnemy = Vector3.Distance(transform.position, enemy.transform.position);
                    if (distanceToEnemy < shortestDistance)
                    {
                        shortestDistance = distanceToEnemy;
                        nearestEnemy = enemy;
                    }
                }
            }

            if (nearestEnemy != null && shortestDistance <= _range)
            {
                _target = nearestEnemy.transform;
            }
            else
            {
                _target = null;
            }
        }

        private void LockOnTarget(Transform target)
        {
            Vector3 dir = transform.position - target.position;
            Quaternion lookRotation = Quaternion.LookRotation(dir);

            foreach (GameObject gun in _gunBodies)
            {
                Vector3 rotation = Quaternion.Lerp(gun.transform.rotation, lookRotation, Time.deltaTime * turnSpeed).eulerAngles;
                gun.transform.rotation = Quaternion.Euler(0f, rotation.y, 0f);
            }
        }

        public bool IsShooting()
        {
            return (_target != null && _canShoot);
        }

        private void Update()
        {
            if (IsShooting())
            {
                LockOnTarget(_target);

                if (_fireCountdown <= 0f)
                {
                    Shoot();
                    _fireCountdown = 1f / fireRate;
                }

                _fireCountdown -= Time.deltaTime;
            }
        }

        private void Shoot()
        {
            foreach (Gun gun in _guns)
            {
                gun.Shoot(_target);

                OnShot.Invoke();
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireSphere(transform.position, _range);
        }

        public void IncreaseRange(int upgradeAmount)
        {
            _range += upgradeAmount;
            GetComponent<SphereCollider>().radius = _range;
        }

        public void IncreaseForce(int upgradeAmount)
        {
            foreach (Gun gun in _guns)
            {
                gun.IncreaseForce(upgradeAmount);
            }
        }
    }
}
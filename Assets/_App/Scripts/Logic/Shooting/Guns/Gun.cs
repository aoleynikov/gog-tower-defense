﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public class Gun : MonoBehaviour
    {
        [SerializeField]
        private Transform _bulletStartPoint;

        [SerializeField]
        private Bullet _bulletPrefab;

        [SerializeField]
        private int _additionalForce = 0;

        public void Shoot(Transform target)
        {
            Bullet bullet = GameObject.Instantiate(_bulletPrefab.gameObject).GetComponent<Bullet>();

            bullet.IncreaseDamage(_additionalForce);
            bullet.SetStartPosition(_bulletStartPoint.position);
            bullet.Seek(target);            
        }

        public void IncreaseForce(int upgradeAmount)
        {
            _additionalForce += upgradeAmount;
        }

    }
}

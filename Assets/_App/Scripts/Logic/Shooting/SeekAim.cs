﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public class SeekAim : MonoBehaviour
    {
        [SerializeField]
        private string[] _tagsToSeek;

        [SerializeField]
        private List<GameObject> _aims = new List<GameObject>();

        [SerializeField]
        private bool _canSeek = false;

        public bool CanSeek { get { return _canSeek; } set { _canSeek = value; } }

        public List<GameObject> Aims { get { return _aims; } }

        private void OnTriggerEnter(Collider other)
        {
            if (ContainsTag(other.tag) && other.GetComponent<TakeDamage>().CanTakeDamage)
            {
                _aims.Add(other.gameObject);
            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (ContainsTag(other.tag))
            {
                for (int i = _aims.Count - 1; i >= 0; i--)
                {
                    if (_aims[i] == other.gameObject)
                    {
                        _aims.RemoveAt(i);
                        return;
                    }
                }
            }
        }

        private bool ContainsTag(string objectTag)
        {
            foreach (string tag in _tagsToSeek)
            {
                if (objectTag.Equals(tag))
                {
                    return true;
                }
            }

            return false;
        }

    }

}

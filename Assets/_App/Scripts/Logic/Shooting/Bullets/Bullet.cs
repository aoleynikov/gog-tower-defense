﻿using UnityEngine;

namespace GOG.TowerDefense
{
    public class Bullet : MonoBehaviour
    {     
        [SerializeField, Tooltip("Speed of bullet")]
        private float _speed = 7;
        [SerializeField, Tooltip("Damage caused by bullet")]
        private int _damage = 50;

        private Transform _target;
        private int _additionalDamage = 0;

        private void Update()
        {
            if (_target == null)
            {
                Destroy(gameObject);
                return;
            }

            Vector3 dir = _target.position - transform.position;
            float distanceThisFrame = _speed * Time.deltaTime;

            if (dir.magnitude <= distanceThisFrame)
            {
                HitTarget();
                return;
            }

            transform.Translate(dir.normalized * distanceThisFrame, Space.World);
            transform.LookAt(_target);

        }


        public void SetStartPosition(Vector3 startPos)
        {
            transform.position = startPos;
        }

        public void Seek(Transform target)
        {
            _target = target;

        }

        private void HitTarget()
        {
            Damage(_target);

            Destroy(gameObject);
        }

        private void Damage(Transform enemy)
        {
            TakeDamage damager = enemy.GetComponent<TakeDamage>();

            if (damager != null)
            {
                damager.Damage(_damage + _additionalDamage);
            }
        }

        public void IncreaseDamage(int amount)
        {
            _additionalDamage += amount;
        }
    }

}

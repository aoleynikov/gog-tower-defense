﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public class WaypointsController : MonoBehaviour, IWayPointController
    {
        [SerializeField]
        private Transform[] _waypoints;

        public Transform[] WayPoints
        {
            get { return _waypoints; }
        }

        void Awake()
        {
            FindWaypoints();
        }

        public void FindWaypoints()
        {
            _waypoints = new Transform[transform.childCount];
            for (int i = 0; i < _waypoints.Length; i++)
            {
                _waypoints[i] = transform.GetChild(i);
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundPlayer : MonoBehaviour
    {
        [SerializeField]
        private bool _playOnAwake;

        private AudioSource _audioSource;
        private AudioClip _audioClip;

        private void Awake()
        {
            _audioSource = gameObject.GetComponent<AudioSource>();
            _audioClip = _audioSource.clip;
        }

        public void PlayOneShot()
        {            
            _audioSource.PlayOneShot(_audioClip);
        }

        public void Play(bool loop)
        {
            Stop();
            _audioSource.loop = loop;
            _audioSource.Play();
        }

        public void Stop()
        {
            _audioSource.Stop();
        }
    }
}

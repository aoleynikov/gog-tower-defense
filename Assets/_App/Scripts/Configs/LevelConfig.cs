﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense {

    [CreateAssetMenu(fileName = "LevelConfig", menuName = "GOG/Level Config")]
    public class LevelConfig : ScriptableObject
    { 
        [Tooltip("Name of scene")]
        public string scenePath;
        [Tooltip("List of level's waves")]
        public List<WaveOnLevelConfig> wavesOnLevel;
        [Tooltip("Waypoint controller (connected to ways prefab)")]
        public WaypointsController waypointsController;
        [Tooltip("Shop configuration of the level")]
        public ShopConfig shopConfig;
        [Tooltip("Player configuration of the level")]
        public PlayerConfig playerConfig;
        [Tooltip("Maximum healt of the base on the level")]
        public int maxBaseLives = 2000;

        
    }

}

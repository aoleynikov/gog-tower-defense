﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "EnemyConfig", menuName = "GOG/Enemy Config")]
    public class EnemyConfig : ScriptableObject
    {
        [Tooltip("Base health decrement when enemy hits it")]
        public int baseHitDamage = 20;
        [Tooltip("Enemy's speed")]
        public float speed = 20;
        [Tooltip("Enemy's turn speed when shooting")]
        public float turnSpeed = 10f;
        [Tooltip("Money bonus player gets when he kills enemy")]
        public int moneyBonusForKill = 50;

    }
}

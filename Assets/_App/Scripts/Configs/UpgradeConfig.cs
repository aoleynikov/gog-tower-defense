﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "UpgradeConfig", menuName = "GOG/Upgrade Config")]
    public class UpgradeConfig : ScriptableObject
    {
        [Tooltip("Upgrade type (e.g. Distance, Force, etc.)")]
        public UpgradeType UpgradeType;
        [Tooltip("Amount of upgrade")]
        public int UpgradeAmount;
        [Tooltip("Price of upgrade")]
        public int UpgradePrice;
    }
}

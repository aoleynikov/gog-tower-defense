﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "GameplaySettingsConfig", menuName = "GOG/GameplaySettings Config")]
    public class GameplaySettingsConfig : ScriptableObject
    {
        [Tooltip("Which gameplay settings exist")]
        public List<GameplaySettings> gameplaySettings;
    }
}

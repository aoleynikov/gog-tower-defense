﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "GridConfig", menuName = "GOG/Grid Config")]
    public class GridConfig : ScriptableObject
    {
        [SerializeField, Tooltip("Width of grid to create")]
        private int _gridWidth = 1;
        [SerializeField, Tooltip("Height of grid to create")]
        private int _gridHeight = 1;
        [SerializeField, Tooltip("Prefab of cell of grid")]
        private GameObject _cellPrefab;
        [SerializeField, Tooltip("Start point of grid")]
        private Vector3 _startGridPoint = new Vector3(0, 0, 0);
        [SerializeField, Tooltip("Step of grid")]
        private Vector3 _step = new Vector3(1, 0, 1);

        private static GridCreator _gridCreator;
        public void CreateGrid()
        {
            if (_gridCreator == null)
            {
                _gridCreator = new GridCreator();
            }
            _gridCreator.CreateGrid(_cellPrefab, _gridWidth, _gridHeight, _startGridPoint, _step);
        }
    }
}

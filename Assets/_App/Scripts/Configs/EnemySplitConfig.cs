﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "EnemySplitConfig", menuName = "GOG/Enemy Split Config")]
    public class EnemySplitConfig : ScriptableObject
    {
        [Tooltip("Which prefab intantiate when enemy is killed")]
        public BaseEnemy splittedPrefab;
        [Tooltip("Amount of enemies instantiated")]
        public int count;
    }
}

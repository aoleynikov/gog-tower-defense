﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "GameConfig", menuName = "GOG/Game Config")]
    public class GameConfig : ScriptableObject
    {
        [Tooltip("Levels in game")]
        public List<LevelConfig> levels;
        [Tooltip("SO of gameplay settings")]
        public GameplaySettingsConfig gameplaySettingsConfig;

    }
}
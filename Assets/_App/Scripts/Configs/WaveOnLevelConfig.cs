﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "WaveConfig", menuName = "GOG/Wave Config")]
    public class WaveOnLevelConfig : ScriptableObject
    {
        [Range(1, 200), Tooltip("Count of enemies in the wave")]
        public int enemiesCount;
        [Range(0.1f, 20), Tooltip("Rate of enemies spawing")]
        public float rate;
        [Range(0, 10), Tooltip("Pause after the wave")]
        public float pause;
        [Tooltip("List of enemies in the wave")]
        public List<Enemy> enemies;
        [Range(1, 10), Tooltip("Enemies' speed multiplier")]
        public float speedMultiplier = 1;
        [Range(0, 100000), Tooltip("Increment of turret price in the wave")]
        public int turretsPriceAdd = 0;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "PlayerConfig", menuName = "GOG/Player Config")]
    public class PlayerConfig : ScriptableObject
    {
        [Tooltip("Start amount of money on the level")]
        public int startMoney = 400;
    }
}

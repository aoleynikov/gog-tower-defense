﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "ShopConfig", menuName = "GOG/Shop Config")]
    public class ShopConfig : ScriptableObject
    {
        [Tooltip("Turrets for selling on the level")]
        public List<Turret> turretsOnLevel;
    }

}

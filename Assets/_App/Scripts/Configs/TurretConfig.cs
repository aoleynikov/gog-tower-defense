﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GOG.TowerDefense
{
    [CreateAssetMenu(fileName = "TurretConfig", menuName = "GOG/Turret Config")]
    public class TurretConfig : ScriptableObject
    {
        [Tooltip("Sprite of turret for the shop")]
        public Sprite shopImage;
        [Tooltip("Price of turret")]
        public int shopPrice = 100;

    }
}
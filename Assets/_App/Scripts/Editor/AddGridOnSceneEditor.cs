﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace GOG.TowerDefense
{
    [CustomEditor(typeof(GridConfig))]
    public class AddGridOnSceneEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();

            GridConfig gridConfig = (GridConfig)target;
            if (GUILayout.Button("Create grid"))
            {
                gridConfig.CreateGrid();
            }
        }
    }
}

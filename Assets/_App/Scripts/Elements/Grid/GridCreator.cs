﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GOG.TowerDefense
{
    public class GridCreator
    {
        public void CreateGrid(GameObject cellPrefab, int gridWidth, int gridHeight, Vector3 startGridPoint, Vector3 step)
        {
            GameObject grid = new GameObject();
            grid.transform.position = startGridPoint;
            grid.name = "Grid_" + gridWidth + "x" + gridHeight;

            Vector3 pos = new Vector3(0, 0, 0);

            for (int i = 0; i < gridWidth; i++)
            {
                pos.x = i * step.x;

                for (int j = 0; j < gridHeight; j++)
                {
                    pos.z = j * step.z;
                    GameObject cell = GameObject.Instantiate(cellPrefab);
                    cell.transform.parent = grid.transform;
                    cell.transform.localPosition = pos;
                    cell.layer = LayerMask.NameToLayer("Placement");

                }

                pos.x = 0;
            }
        }
    }
}

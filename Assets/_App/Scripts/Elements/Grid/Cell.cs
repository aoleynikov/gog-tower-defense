﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

namespace GOG.TowerDefense
{
    /// <summary>
    /// Element to place turret on
    /// </summary>
    public class Cell : MonoBehaviour
    {
        
        [SerializeField]
        private Color _hoverColor;

        private Color _startColor;
        private Material _material;
        private bool _canPlace = true;
        private IPlaceable _placeable;
        private bool _turretOver = false;
        private float _turretOverLastTime = 0;
        private float _turretOverDelayTime = 0.1f;

        public bool CanPlace
        {
            get
            {
                return _canPlace;
            }

            set
            {
                _canPlace = value;
            }
        }

        private void Start()
        {
            _material = gameObject.GetComponent<Renderer>().material;
            _startColor = _material.color;
        }

        private void Update()
        {
            if (Time.time - _turretOverLastTime > _turretOverDelayTime)
            {
                _turretOver = false;
            }

            if (_turretOver)
            {
                _material.color = _hoverColor;
            }
            else
            {
                _material.color = _startColor;
            }
        }

        public void OnTurretOver()
        {
            _turretOver = true;
            _turretOverLastTime = Time.time;
        }

        public bool OnTurretTryPlace(IPlaceable placeable)
        {
            if (_canPlace)
            {
                _placeable = placeable;

                _canPlace = false;

                return true;
            }

            return false;
        }

        public void RemoveObjectFromCell()
        {
            CanPlace = true;
        }
    }

}
